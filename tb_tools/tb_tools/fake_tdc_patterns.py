from tb_tools.tdc_input import measure_tag

# Note: the patterns was made thinking in an ISERDESE2 with a clock of 10ns

TEST_PATTERNS = {
     # fast_small_pulses: test the TDC with a signal composed of fast small pulses.
    # The input signal frequency is 4 times the frequency of the system clock
    'few_fast_small_pulses': {
        'input_period_ns': 2.5,
        'input': [
            0,0,0,0,    0,0,0,1,    0,0,1,0,    0,0,1,1,    0,1,0,0,    0,1,0,1,
            0,1,1,0,    0,1,1,1,    0,1,0,0,    1,0,0,1,    0,0,1,1,    0,0,1,1,
        ],
        'expected_ram_data': [
            {'clock_cycle_count': 1,    'data': 0b0001, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 2,    'data': 0b0010, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 3,    'data': 0b0011, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 4,    'data': 0b0100, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 5,    'data': 0b0101, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 6,    'data': 0b0110, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 7,    'data': 0b0111, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 8,    'data': 0b0100, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 9,    'data': 0b1001, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 10,   'data': 0b0011, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 11,   'data': 0b0011, 'tag': measure_tag.SMALL_PULSE},
        ]
    },
    # fast_small_pulses: test the TDC with a signal composed of fast small pulses.
    # The input signal frequency is 4 times the frequency of the system clock
    'fast_small_pulses': {
        'input_period_ns': 2.5,
        'input': [
            0,0,0,0,    0,0,0,1,    0,0,1,0,    0,0,1,1,    0,1,0,0,    0,1,0,1,
            0,1,1,0,    0,1,1,1,    0,1,0,0,    1,0,0,1,    0,0,1,1,    0,1,1,1,
            0,1,1,0,    0,1,0,0,    0,0,0,0,    0,0,0,0,    0,1,1,1,    0,1,0,0
        ],
        'expected_ram_data': [
            {'clock_cycle_count': 1,    'data': 0b0001, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 2,    'data': 0b0010, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 3,    'data': 0b0011, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 4,    'data': 0b0100, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 5,    'data': 0b0101, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 6,    'data': 0b0110, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 7,    'data': 0b0111, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 8,    'data': 0b0100, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 9,    'data': 0b1001, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 10,   'data': 0b0011, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 11,   'data': 0b0111, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 12,   'data': 0b0110, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 13,   'data': 0b0100, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 16,   'data': 0b0111, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 17,   'data': 0b0100, 'tag': measure_tag.SMALL_PULSE},
        ]
    },
    # small_pulses: test the TDC with a signal composed of small pulses.
    # The input signal frequency is 2 times the frequency of the system clock.
    'small_pulses': {
        'input_period_ns': 5,
        'input': [
            0,0,    
            0,0,    
            0,0,    
            0,1,    # clock cycle 3
            0,0,    
            1,0,    # clock cycle 5
            0,0,    
            1,1,    # clock cycle 7
            0,1,    # clock cycle 8
            0,0,    
            0,1,    # clock cycle 10
            0,1,    # clock cycle 11
            0,1,    # clock cycle 12
            1,0,    # clock cycle 13
            0,1,    # clock cycle 14
            1,1,    # clock cycle 15
            0,1,    # clock cycle 16
            0,0,    
            1,0,    # clock cycle 18
            0,1,    # clock cycle 19
            0,0,    
            1,1,    # clock cycle 21
            0,1,    # clock cycle 22
            1,1,    # clock cycle 23
            0,1,    # clock cycle 24
            1,0,    # clock cycle 25
            0,1,    # clock cycle 26
            0,0,    
            0,0,    
            0,0,    
            0,0,    
            0,0,    
            0,1,    # clock cycle 32
            1,1,    # clock cycle 33
            0,1,    # clock cycle 34
            0,0     
        ],
        'expected_ram_data': [
            {'clock_cycle_count': 3,    'data': 0b0011, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 5,    'data': 0b1100, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 7,    'data': 0b1111, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 8,    'data': 0b0011, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 10,   'data': 0b0011, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 11,   'data': 0b0011, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 12,   'data': 0b0011, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 13,   'data': 0b1100, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 14,   'data': 0b0011, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 15,   'data': 0b1111, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 16,   'data': 0b0011, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 18,   'data': 0b1100, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 19,   'data': 0b0011, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 21,   'data': 0b1111, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 22,   'data': 0b0011, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 23,   'data': 0b1111, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 24,   'data': 0b0011, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 25,   'data': 0b1100, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 26,   'data': 0b0011, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 32,   'data': 0b0011, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 33,   'data': 0b1111, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 34,   'data': 0b0011, 'tag': measure_tag.SMALL_PULSE},
        ]
    },
    # fast_long_pulses: test the TDC with a signal composed of fast long pulses.
    # The input signal frequency is 4 times the frequency of the system clock
    'fast_long_pulses': {
        'input_period_ns': 2.5,
        'input': [
            0,1,1,1,    1,1,1,1,    1,0,0,0,    0,0,0,1,    1,1,1,1,    1,1,1,1,
            1,1,1,1,    1,1,1,0,    0,0,0,0,    1,1,1,1,    1,0,0,0,    0,1,1,1,
            1,1,1,1,    1,1,1,1,    1,0,0,1,    1,1,1,1,    1,0,1,1,    1,1,1,1,
            1,1,1,1,    1,1,1,1,    1,0,0,0,    0,0,1,1,    1,1,1,1,    1,1,1,0
        ],
        'expected_ram_data': [
            {'clock_cycle_count': 0,    'data': 0b0111, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 2,    'data': 0b1000, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 3,    'data': 0b0001, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 7,    'data': 0b1110, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 9,    'data': 0b1111, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 10,   'data': 0b1000, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 11,   'data': 0b0111, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 14,   'data': 0b1001, 'tag': measure_tag.LONG_PULSE_ENDING_LONG_STARTING},
            {'clock_cycle_count': 16,   'data': 0b1011, 'tag': measure_tag.LONG_PULSE_ENDING_LONG_STARTING},
            {'clock_cycle_count': 20,   'data': 0b1000, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 21,   'data': 0b0011, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 23,   'data': 0b1110, 'tag': measure_tag.LONG_PULSE_ENDING},
        ]
    },
    # long_pulses: test the TDC with a signal composed of long pulses.
    # The input signal frequency is 2 times the frequency of the system clock.
    'long_pulses': {
        'input_period_ns': 5,
        'input': [
            0,1,    # clock cycle 0
            1,1,    
            1,0,    # clock cycle 2
            0,1,    # clock cycle 3
            1,1,    
            1,1,    
            1,1,    
            1,0,    # clock cycle 7
            0,0,    
            1,1,    # clock cycle 9
            1,0,    # clock cycle 10
            0,1,    # clock cycle 11
            1,1,    
            1,1,    
            1,0,    # clock cycle 14
            1,1,    # clock cycle 15
            1,1,    
            1,1,    
            1,0,    # clock cycle 18
        ],
        'expected_ram_data': [
            {'clock_cycle_count': 0,    'data': 0b0011, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 2,    'data': 0b1100, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 3,    'data': 0b0011, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 7,    'data': 0b1100, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 9,    'data': 0b1111, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 10,   'data': 0b1100, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 11,   'data': 0b0011, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 14,   'data': 0b1100, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 15,   'data': 0b1111, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 18,   'data': 0b1100, 'tag': measure_tag.LONG_PULSE_ENDING},
        ]
    },
    # fast_combinated_pulses: test the TDC with a signal composed of fast small and long pulses.
    # The input signal frequency is 4 times the frequency of the system clock
    'fast_intercalated_pulses': {
        'input_period_ns': 2.5,
        'input': [
            0,1,1,1,    1,1,1,1,    1,0,1,0,    0,0,0,1,    0,0,1,0,    1,1,1,1,
            1,1,1,1,    1,1,1,1,    1,1,1,0,    0,0,0,0,    0,0,0,0,    0,1,1,0,
            1,1,1,1,    1,0,0,1,    1,0,0,1,    1,1,1,1,    1,1,1,1,    1,1,1,0,
            1,1,1,1,    1,1,1,1,    1,0,0,0,    0,0,1,1,    1,1,1,1,    1,1,1,0
        ],
        'expected_ram_data': [
            {'clock_cycle_count': 0,    'data': 0b0111, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 2,    'data': 0b1010, 'tag': measure_tag.LONG_PULSE_ENDING_SMALL_APPEARS},
            {'clock_cycle_count': 3,    'data': 0b0001, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 4,    'data': 0b0010, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 5,    'data': 0b1111, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 8,    'data': 0b1110, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 11,   'data': 0b0110, 'tag': measure_tag.SMALL_PULSE},
            {'clock_cycle_count': 12,   'data': 0b1111, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 13,   'data': 0b1001, 'tag': measure_tag.LONG_PULSE_ENDING_LONG_STARTING},
            {'clock_cycle_count': 14,   'data': 0b1001, 'tag': measure_tag.LONG_PULSE_ENDING_LONG_STARTING},
            {'clock_cycle_count': 17,   'data': 0b1110, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 18,   'data': 0b1111, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 20,   'data': 0b1000, 'tag': measure_tag.LONG_PULSE_ENDING},
            {'clock_cycle_count': 21,   'data': 0b0011, 'tag': measure_tag.LONG_PULSE_STARTING},
            {'clock_cycle_count': 23,   'data': 0b1110, 'tag': measure_tag.LONG_PULSE_ENDING},
        ]
    },
}
