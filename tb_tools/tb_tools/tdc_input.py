from cocotb.triggers import Timer
from enum import IntEnum


class measure_tag(IntEnum):
    SMALL_PULSE                                     = 1 #0b001
    LONG_PULSE_STARTING                             = 2 #0b010
    LONG_PULSE_STARTING_SMALL_APPEARS               = 3 #0b011
    LONG_PULSE_ENDING                               = 4 #0b100
    LONG_PULSE_ENDING_SMALL_APPEARS                 = 5 #0b101
    LONG_PULSE_ENDING_LONG_STARTING                 = 6 #0b110


async def simulate_serdes_output(dut_signal, values: list, clock_period_ns: int):
    """
    Simulate serdes output in a DUT signal given as parameter.

    Parameters
    ----------
    dut_signal : 
        The DUT signal where the serdes output will be simulated.

    values : list
        A list with the values of the input signal.

    clock_period_ns : int
        Clock period in nanosecond.

    """
    dut_signal.value = 0b0000

    await Timer(1, 'ps')    
    for val in values:
        dut_signal.value = val
        await Timer(clock_period_ns, "ns")

    dut_signal.value = 0b0000


async def simulate_input_signal(dut_signal, values: list, clock_period_ns: int):
    """
    Simulate input signal that must be feeded into the DUT in a DUT signal given as parameter.

    Parameters
    ----------
    dut_signal : 
        The DUT signal where the input signal will be simulated.

    values : list
        A list with the values of the input signal.

    clock_period_ns : int
        Clock period in nanosecond.
    """
    dut_signal.value = 0b0

#    await Timer(1, 'ps')
    for val in values:
        dut_signal.value = val
        await Timer(clock_period_ns, "ns")

    dut_signal.value = 0b0


async def generate_multiphase_clocks(dut, period_ns):
    """ Generates 4 clocks with 45 grades of phase shift between them """
    # pre-construct triggers for performance
    phase_shifts = period_ns/4
    dut._log.info('generating clocks of {}MHz ({}ns) with phase shifts of {}us'.format(round(1000/period_ns), period_ns, phase_shifts))
    while True:
        dut.i_clk.value = 1
        dut.i_oclk.value = 1
        dut.i_clkb.value = 0
        dut.i_oclkb.value = 0
        await Timer(phase_shifts, units='ns')
        dut.i_clk.value = 0
        dut.i_oclk.value = 1
        dut.i_clkb.value = 1
        dut.i_oclkb.value = 0
        await Timer(phase_shifts, units='ns')
        dut.i_clk.value = 0
        dut.i_oclk.value = 0
        dut.i_clkb.value = 1
        dut.i_oclkb.value = 1
        await Timer(phase_shifts, units='ns')
        dut.i_clk.value = 1
        dut.i_oclk.value = 0
        dut.i_clkb.value = 0
        dut.i_oclkb.value = 1
        await Timer(phase_shifts, units='ns')