from enum import IntEnum
from typing import Union
import re
from math import ceil

TDC_CMD_ID_WIDTH_BYTES = 3
TDC_CMD_DATA_WIDTH_BYTES = 61

# ID of the TDC commands executors 
class CMD_EXECUTOR_ID(IntEnum):
    READ_CRONO_TDC_CORE_VERSION = 1
    MEASURE                     = 2
    READ_TIME_WINDOWS_US        = 4
    WRITE_TIME_WINDOWS_US       = 8
    READ_CHANNELS_ENABLED       = 16
    WRITE_CHANNELS_ENABLED      = 32
    READ_CHANNELS_AVAILABLE     = 64
    READ_CHANNELS_DATA          = 128

# ID of the TDC commands
class CMD_ID(IntEnum):
    READ_CRONO_TDC_CORE_VERSION = 1
    MEASURE                     = 2
    READ_TIME_WINDOWS_US        = 3
    WRITE_TIME_WINDOWS_US       = 4
    READ_CHANNELS_ENABLED       = 5
    WRITE_CHANNELS_ENABLED      = 6
    READ_CHANNELS_AVAILABLE     = 7
    READ_CHANNELS_DATA          = 8


def build_tdc_command(cmd_id: int, cmd_data: Union[int, bytes], bytes_rep: bool = False) -> Union[int, bytes]:
    """ 
    Build a TDC command with a determined ID and DATA and returns it as an integer
    or as bytes.
    """
    if isinstance(cmd_data, bytes):
        cmd_data_int = int.from_bytes(cmd_data, 'big')
    else:
        cmd_data_int = cmd_data

    int_rep = cmd_id | (cmd_data_int << TDC_CMD_ID_WIDTH_BYTES*8)
    if bytes_rep:
        return int_rep.to_bytes(ceil(int_rep.bit_length()/8), 'big')
    else:
        return int_rep


def read_crono_tdc_version(crono_tdc_core_version_pkg: str) -> Union[str, None]:
    with open(crono_tdc_core_version_pkg, 'r') as f:
        data = f.readlines()

    githash = None
    date = None

    for line in data:
        if 'CRONO_TDC_CORE_GITHASH' in line:
            match = re.search("(\".*\")", line)
            if match:
                githash = match.group().strip('""')
        
        if githash:
            return githash