from cocotb.triggers import Timer, FallingEdge, with_timeout


async def send_bytes_through_uart(  dut_rx_serial, logger,
                                    baudrate: int, data_to_send: bytes,
                                    stop_bits: int = 1):
    """
    Simulates a uart sending data through a serial port at a given baudrate.

    Parameters
    ----------
    dut_rx_serial : 
        Serial input port of the dut.

    logger : 
        Logger object (`dut._log` could be used).

    baudrate : int
        Speed of the comunication (bauds/s).

    baudrate : bytes
        The bytes that will be sent.

    stop_bits : int
        The quantity of stop bits that will be used.
    """
    bit_t = Timer(int(1e9/baudrate), 'ns')
    stop_bit_t = Timer(int(1e9/baudrate*stop_bits), 'ns')

    logger.info(f'start sending bytes {data_to_send} (bit_t: {int(1e9/baudrate)} ns)')

    for b in data_to_send:
        logger.info(f'sending byte 0x{b:2X}: {b:c}')
        # sending start bit
        dut_rx_serial.value = 0
        await bit_t

        # data bits
        for k in range(8):
            dut_rx_serial.value = b & 1
            b >>= 1
            await bit_t

        # stop bit
        dut_rx_serial.value = 1
        await stop_bit_t


async def receive_bytes_through_uart(   dut_tx_serial, logger,
                                        baudrate: int, bytes_qty: int,
                                        stop_bits: int = 1,
                                        assert_timeout_us: int = 1000) -> bytes:
    """
    Simulates a uart receiving data through a serial port at a given baudrate.

    Parameters
    ----------
    dut_tx_serial : 
        Serial ouput port of the dut.

    baudrate : int
        Speed of the comunication (bauds/s).

    bytes_qty : int
        The quantity of bytes that will be received.

    stop_bits : int
        The quantity of stop bits that will be used.

    assert_timeout_us : int
        The test will assert if a byte is not received in `assert_timeout_us`
        microseconds.
    """
    half_bit_t = Timer(int(1e9/baudrate/2), 'ns')
    bit_t = Timer(int(1e9/baudrate), 'ns')
    stop_bit_t = Timer(int(1e9/baudrate*stop_bits), 'ns')

    bytes_received = 0
    bytes_received_counter = 0

    for b in range(bytes_qty):
        await with_timeout(FallingEdge(dut_tx_serial), assert_timeout_us, 'us')

        # start bit
        await half_bit_t

        # data bits
        b = 0
        for k in range(8):
            await bit_t
            b |= bool(dut_tx_serial.value.integer) << k

        bytes_received = bytes_received << 8
        bytes_received |= b & 0xFF

        # stop bit
        await stop_bit_t
    
    bytes_received_counter+=1
    logger.info(f'byte 0x{bytes_received & 0xFF:02X} received...')

    return bytes_received.to_bytes(bytes_received_counter, 'big')
