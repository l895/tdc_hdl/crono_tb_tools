from pickle import FRAME
from typing import Union
from crc import CrcCalculator, Crc16

from tb_tools.uart import receive_bytes_through_uart, send_bytes_through_uart

FRAME_DELIMITER_CHAR = 0x7E
ESCAPE_CHAR = 0x7D

CONTROL_CHARS = [ESCAPE_CHAR, FRAME_DELIMITER_CHAR]

def build_hdlc_lite_frame(data: bytes) -> bytes:
    """
    Build a hdlc-lite frame with a certain data field. 
    This method will escape any char that must be escaped and
    then will compute the CRC16-CCITT of the data too (after
    escaping if it is needed).

    Parameters
    ----------
    data : bytearray
        The data that wants to be sent in the frame.
    """
    frame_data = []
    counter = 0
    for b in data:
        if b in CONTROL_CHARS:
            frame_data.append(ESCAPE_CHAR)
            frame_data.append(b ^ 0x20)
        else:
            frame_data.append(b)
        counter+=1

    crc_calculator = CrcCalculator(Crc16.CCITT)
    checksum = crc_calculator.calculate_checksum(frame_data)
    data_crc = [0xFF & (checksum >> 8), 0xFF & checksum]

    frame = bytearray([FRAME_DELIMITER_CHAR]+frame_data+data_crc+[FRAME_DELIMITER_CHAR])

    return frame


async def send_frame_through_uart( dut_rx_serial, logger, frame: bytes, baudrate: int, stop_bits: int = 1):
    """
    Simulate a UART sending a hdlc frame at a given baudrate.

    Parameters
    ----------
    dut_rx_serial : 
        Serial input port of the dut.

    frame : bytes
        Frame to send.

    baudrate : int
        Speed of the comunication (bauds/s).

    stop_bits : int
        The quantity of stop bits that will be used.

    """
    await send_bytes_through_uart(dut_rx_serial, logger, baudrate, frame, stop_bits)


async def receive_frame_through_uart(   dut_tx_serial, logger,
                                        baudrate: int, 
                                        stop_bits: int = 1) -> Union[dict, None]:
    """
    Simulate a uart receiving a hdlc frame at a given baudrate and returns
    only the data field.

    Parameters
    ----------
    dut_tx_serial : 
        Serial ouput port of the dut.

    baudrate : int
        Speed of the comunication (bauds/s).

    stop_bits : int
        The quantity of stop bits that will be used.
    """
    frame_content = bytearray()

    ## first receive the start frame delimiter
    rx_byte = await receive_bytes_through_uart(dut_tx_serial,logger,baudrate,1,stop_bits)
    while( rx_byte != b'\x7E' ):
        logger.info(f'received a byte different from frame delimiter: {rx_byte}!')
        rx_byte = await receive_bytes_through_uart(dut_tx_serial,logger,baudrate,1,stop_bits)

    logger.info('start frame delimiter received!')

    ## store data until the end frame delimiter is received
    rx_byte = await receive_bytes_through_uart(dut_tx_serial,logger,baudrate,1,stop_bits)
    while( rx_byte != b'\x7E' ):
        logger.info(f'received a data byte: {rx_byte}!')
        frame_content.extend(rx_byte)
        rx_byte = await receive_bytes_through_uart(dut_tx_serial,logger,baudrate,1,stop_bits)

    logger.info('end frame delimiter received!')

    if len(frame_content) < 2:
        ret_val = None
        logger.info('invalid frame format! len < 2')
    else:
        ret_val = {'data': bytes(frame_content[:-2]), 'crc': bytes(frame_content[-2:])}
        logger.info(f"frame data: {ret_val['data']}, frame CRC: {ret_val['crc']}")

    return ret_val