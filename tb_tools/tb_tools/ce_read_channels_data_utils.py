from cocotb.triggers import Timer, RisingEdge, Join, with_timeout
import cocotb
from math import floor

from tb_tools.utils import int_to_one_hot_encoding

CHANNEL_ID_WIDTH_BYTES=2 # Size in bytes of the CHANNEL_ID in the tx buffer
DATA_REGISTERS_QTY_WIDTH_BYTES=1 # Size in bytes of the number of data registers in the tx buffer

I_TX_DATA_DONE_ACTIVE_TIME_MULTIPLIER=2 # `i_tx_data_done` active time based on clock cycles

async def simulate_channels_ram(dut, channels_ram: dict):
    """
    Simulate channels RAM. Checks if the DUT selects a given address
    of a given channel and exposes that data in it's input port.
    Take in count that the data is exposed in the next clock cycle.
    So, if we set the address 0 in the clock cycle `i`, we will have
    the data of address 0 in the clock cycle `i+1`.

    Parameters
    ----------
    channels_ram : dict
        Dictionary that indicates the data stored in channels RAM:
        {
            0: <array with the values stored in channel 0 RAM>,
            1: <array with the values stored in channel 1 RAM>,
            ...
        }.
    """
    dut._log.info('channels RAM simulator starting...')


    next_cycle_val = 0
    while True:
        await RisingEdge(dut.i_clk)
        await Timer(1, 'ps')
        dut.i_read_chn_data.value = next_cycle_val

        for ch in channels_ram:
            if int_to_one_hot_encoding(ch) == dut.o_read_chn_sel_mask.value.integer:
                if dut.o_read_chn_addr.value.integer > len(channels_ram[ch]):
                    dut._log.info(f"address {dut.o_read_chn_addr.value.integer} is bigger than available data in RAM ({len(channels_ram[ch])}) (address > lwa)")
                else:
                    dut.i_read_chn_lwa.value = len(channels_ram[ch])-1 ## TODO: check if this must be -1
                    next_cycle_val = channels_ram[ch][dut.o_read_chn_addr.value.integer]
                    dut._log.info(f"address {dut.o_read_chn_addr.value.integer}, next cycle value: 0x{next_cycle_val:X}")
                    break


def calculate_expected_tx_buffers(channels_ram: dict,
        channel_data_reg_width: int, tx_buffer_width_bytes: int) -> dict:
    """
    Calculate the expected tx_buffers that the DUT must build
    for a given set of channels with some RAM stored inside.

    Parameters
    ----------
    channels_ram : dict
        Dictionary that indicates the data stored in channels RAM:
        {
            0: <channel 0 array with the values stored in RAM>,
            1: <channel 1 array with the values stored in RAM>,
            ...
        }.

    channel_data_reg_width : int
        Size in bits of channels data registers.

    tx_buffer_width_bytes : int
        Size in bytes of DUT tx buffer. This must be the buffer width that
        takes in count ALL! (data registers, channel ID and registers qty).

    Returns
    ----------
    dict
        Dictionary with the expected tx buffer from DUT for given channels:
        {
            0: <channel 0 tx_buffers list>,
            1: <channel 1 tx_buffers list>,
            ...
        }.
    """
    data_buffer_width = tx_buffer_width_bytes*8
    only_data_regs_buffer_width = (tx_buffer_width_bytes-CHANNEL_ID_WIDTH_BYTES-DATA_REGISTERS_QTY_WIDTH_BYTES)*8
    print(f"building expected tx buffers...\ndata buffer width: {data_buffer_width} [bits]\nchannel_data_reg_width: {channel_data_reg_width}\ndata registers per buffer {floor(only_data_regs_buffer_width/channel_data_reg_width)}...")

    tx_buffers = dict()
    for ch in channels_ram:
        tx_buffers[ch] = list()
        print(f"building expected tx buffer for channel {ch} ({channels_ram[ch]})...")

        data_registers_counter = 0
        while data_registers_counter < len(channels_ram[ch]):
            data_registers_in_buff = 0
            tx_buff = 0
            left_shift_data_reg = only_data_regs_buffer_width-channel_data_reg_width
            while left_shift_data_reg >= 0:
                data_registers_in_buff += 1
                tx_buff |= (channels_ram[ch][data_registers_counter] << left_shift_data_reg)

                data_registers_counter += 1
                print(f"ch: {ch}, tx buffer at the moment: {tx_buff:0X} (reg num {data_registers_counter}/{len(channels_ram[ch])}) (shift: {left_shift_data_reg}, data regs in buff: {data_registers_in_buff})")
                if data_registers_counter >= len(channels_ram[ch]):
                    break
                else:
                    left_shift_data_reg = only_data_regs_buffer_width-(data_registers_in_buff+1)*channel_data_reg_width

            print(f"adding channel ID {ch} and data registers in buffer qty {data_registers_in_buff} to tx buffer...")
            tx_buff |=  (ch << (tx_buffer_width_bytes-CHANNEL_ID_WIDTH_BYTES)*8) | \
                        (data_registers_in_buff << (tx_buffer_width_bytes-CHANNEL_ID_WIDTH_BYTES-DATA_REGISTERS_QTY_WIDTH_BYTES)*8)

            tx_buffers[ch].append(tx_buff)
            print(f"ch: {ch}, buffer: {tx_buff:X}, data regs qty: {data_registers_in_buff}")

    return tx_buffers


async def simulate_tx_data_done_signal(dut, active_time_ns, clock_period_ns):
    """ Wait until o_tx_data_valid is set, wait one clock cycle and set 
    the i_tx_data_done signal. """
    dut._log.info(f"i_tx_data_done_signal_simulator: starting...")
   
    while True:
        while dut.o_tx_data_valid.value.integer == 0:
            await Timer(clock_period_ns, 'ns')

        dut._log.info(f"i_tx_data_done_signal_simulator: setting i_tx_data_done high...")
        await Timer(clock_period_ns, 'ns')
        dut.i_tx_data_done.value = 1
        await Timer(active_time_ns, 'ns')
        dut.i_tx_data_done.value = 0


async def data_buffers_validator(dut, channels_ram: dict, clock_period_ns: int,
        channel_data_reg_width: int, tx_buffer_width_bytes: int,
        expected_o_tx_data_valid: int):
    """
    Coroutine that verify if the tx buffers emitted by the DUT are valid
    based on channels RAM data. This coroutine will stay alive untill
    all tx buffers are received and the DUT sends the end-message.

    Parameters
    ----------
    channels_ram : dict
        Dictionary that indicates the data stored in channels RAM:
        {
            0: <channel 0 array with the values stored in RAM>,
            1: <channel 1 array with the values stored in RAM>,
            ...
        }.

    clock_period_ns : int
        The period of the system clock.

    channel_data_reg_width : int
        Size in bits of the channels data registers.

    tx_buffer_width_bytes : int
        Size in bytes of DUT tx buffer.
    """
    dut._log.info('data buffer validator starting...')
    expected_tx_buffers = calculate_expected_tx_buffers(channels_ram=channels_ram,
        channel_data_reg_width=channel_data_reg_width,
        tx_buffer_width_bytes=tx_buffer_width_bytes)
    
    # dictionary that will store the actual expected tx buffer
    expected_tx_buffers_ptr = dict()
    for k in list(expected_tx_buffers.keys()):
        expected_tx_buffers_ptr[k] = 0

    wait_more_data = True
    for k in list(expected_tx_buffers.keys()):
        if expected_tx_buffers_ptr[k] < len(expected_tx_buffers[k]):
            wait_more_data = True
    
    dut._log.info(f'data_buffers_validator: expected_tx_buffers_ptr: {expected_tx_buffers_ptr}...')
    dut._log.info(f'data_buffers_validator: expected_tx_buffers: {expected_tx_buffers}...')

    cocotb.start_soon(simulate_tx_data_done_signal(dut, clock_period_ns*I_TX_DATA_DONE_ACTIVE_TIME_MULTIPLIER, clock_period_ns))

    while True:
        if (dut.o_tx_data_valid.value.integer != 0):
            if dut.o_tx_data.value.buff[-7:] == b'enaread':
                dut._log.info("\'enabled channels fully read\' received!")
                while (dut.o_tx_data_valid.value.integer != 0):
                    await Timer(clock_period_ns, 'ns')

                assert wait_more_data == False, 'end message received but tx buffers left to send!'
                break
            else:
                decoded_channel = int.from_bytes(dut.o_tx_data.value.buff[0:2], 'big')
                assert decoded_channel in list(expected_tx_buffers.keys()), f"decoded channel ({decoded_channel}) is not in expected channel list"

                data_registers_qty = int.from_bytes(dut.o_tx_data.value.buff[2:3], 'big')

                dut._log.info(f"data_buffers_validator: checking if channel {decoded_channel} buffer ({dut.o_tx_data.value.integer:X}) is valid (data regs in buffer = {data_registers_qty})...")
                dut._log.info(f'data_buffers_validator: expected_tx_buffers_ptr: {expected_tx_buffers_ptr}...')
                dut._log.info(f'data_buffers_validator: expected_tx_buffers: {expected_tx_buffers}...')
                try:
                    assert expected_tx_buffers[decoded_channel][expected_tx_buffers_ptr[decoded_channel]] == dut.o_tx_data.value.integer, f"buffer is not the expected one! {expected_tx_buffers[decoded_channel][expected_tx_buffers_ptr[decoded_channel]]:X} != {dut.o_tx_data.value.integer:X}"
                except IndexError as excp:
                    dut._log.error(f"index error! decoded_channel: {decoded_channel}, expected_tx_buffers_ptr: {expected_tx_buffers_ptr}, ptr: {expected_tx_buffers_ptr[decoded_channel]}")

                dut._log.info(f"data_buffers_validator: buffer it is valid!")

                # check if data valid value is correct
                dut._log.info(f"checking if o_tx_data_valid is valid (channel_data_reg_width = {channel_data_reg_width}, data_registers_qty = {data_registers_qty})")
                assert dut.o_tx_data_valid.value.integer == (expected_o_tx_data_valid), f"data valid have not the expected value: {dut.o_tx_data_valid.value.integer} != {expected_o_tx_data_valid}"

                expected_tx_buffers_ptr[decoded_channel] += 1
                while (dut.o_tx_data_valid.value.integer != 0):
                    await Timer(clock_period_ns, 'ns')

                wait_more_data = False
                for k in list(expected_tx_buffers.keys()):
                    if expected_tx_buffers_ptr[k] < len(expected_tx_buffers[k]):
                        wait_more_data = True
                        dut._log.info('data_buffers_validator: waiting for more data...')
                

        await Timer(clock_period_ns, 'ns')

    dut._log.info('data buffer validator ending...')