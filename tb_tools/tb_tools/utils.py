from cocotb.triggers import Timer

async def reset_dut(sync_reset, duration, time_unit):
    """
    Put in reset state the DUT for a certain time interval 
    and unreset it then.

    Parameters
    ----------
    sync_reset : cocotb.handle.ModifiableObject
        The reset signal of the DUT.

    duration : int
        The width of the reset pulse.

    time_unit: str
        The time unit of the duration parameter (ns, us, etc...)
    """
    sync_reset._log.info("Touching the reset button of the DUT! ({}{})".format(duration, time_unit))
    sync_reset.value = 1
    await Timer(duration, units=time_unit)
    sync_reset.value = 0
    sync_reset._log.info("The reset was completed!")


def one_hot_encoding_to_int(one_hot:int) -> int:
    """
    Transform a one-hot encoding mask into an integer number.
    Takes as 0 the rightmost bit and 31 the leftmost bit.

       31 30 29 28        3  2  1  0
      |  |  |  |  | ... |  |  |  |  |

    i.e.:
     00000000000000000000000000000001 => 0
     00000000000000000000000000000010 => 1
     00000000000000000000000000000100 => 2
     00000000000000000000000000001000 => 3
    """
    ret = 0
    for i in range(32):
        if ((one_hot >> i) & 1) != 0:
            ret = i
    return ret


def int_to_one_hot_encoding(number:int) -> int:
    """
    Transform a number to one-hot encoding.
    Takes as 0 the rightmost bit and 31 the leftmost bit.

       31 30 29 28        3  2  1  0
      |  |  |  |  | ... |  |  |  |  |

    i.e.:
     0 => 00000000000000000000000000000001
     1 => 00000000000000000000000000000010
     2 => 00000000000000000000000000000100
     3 => 00000000000000000000000000001000
    """
    ret = 0
    if number > 31:
        ret = 0b10000000000000000000000000000000
    else:
        ret = 1 << number

    return ret

async def block_until_signal(signal, expected_value, clock_period_ns):
    while signal.value != expected_value:
        await Timer(clock_period_ns, 'ns')

async def block_until_signal_is_different_from_value(signal, value, clock_period_ns):
    while signal.value == value:
        await Timer(clock_period_ns, 'ns')
