import shlex
from subprocess import check_output, CalledProcessError
from setuptools import setup, find_packages
import re
from typing import Union

long_description =  "Package with useful functions to test build testbenchs in cocotb for " + \
                    "Crono TDC Core. There are classes with the definitions of the commands " + \
                    "of the core and another generic functions that could be used to reset a dut."

setup(
    name="crono_tdc_core_testbenchs_tools",
    version='0.0.dev1', ## we really will not use this i think, is just a practical package to share things between testbenchs
    description="Useful tools to create testbenchs in cocotb for Crono TDC Core",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/l895/tdc_hdl/crono_tdc_core",
    author="Julian Rodriguez",
    author_email="jnrodriguez@estudiantes.unsam.edu.ar",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        'Operating System :: Linux',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Utilities',
    ],
    packages=find_packages(where="tb_tools"),
    package_dir={"": "tb_tools"},
    python_requires=">=3.7, <4",
    install_requires=[ "cocotb", "pytest", "crc==1.2.0"],
)