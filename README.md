# Crono TDC Test Bench Tools

This python library was designed to be used in Crono TDC Cocotb Testbenchs.

## Install

This package is pip-installable. So, in order to install it just execute the following command:

```
pip install git+https://gitlab.com/l895/tdc_hdl/crono_tb_tools
```